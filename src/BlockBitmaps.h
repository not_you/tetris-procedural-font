bool square [4] [4]
{
{true,true,false,false},
{true,true,false,false},
{false,false,false,false},
{false,false,false}
};
const bool l_shape [4] [4]
{
{true,true,false,false},
{true,false,false,false},
{true,false,false,false},
{false,false,false,false}
};
const bool i_shape [4] [4]
{
{true,true,true,true},
{false,false,false,false},
{false,false,false,false},
{false,false,false,false}
};
const bool s_shape [4] [4]
{
{false,true,false,false},
{true,true,false,false},
{true,false,false,false},
{false,false,false,false}
};

const bool half_cross [4] [4]
{
{true,true,true,false},
{false,true,false,false},
{false,false,false,false},
{false,false,false,false}
};

bool [4] [4] modBlock(bool block [4][4],bool mirror_x, int rot){
	bool tmp_block [4][4];
	if (mirror_x){
		for(int i=0;i<4;i++){
			for(int j=0; j<4; j++){
				tmp_block[i][j]=block[i][3-j];
			}
		}
		block=tmp_block;
	}
	if(rot>0){
		for(int i=0;i<4;i++){
			for(int j=0; j<4; j++){
				switch(rot){
					case 1:
						tmp_block[i][j]=block[3-j][i];
						break;
					case 2;
						tmp_block[i][j]=block[3-i][3-j];
						break;
					case 3:
						tmp_block[i][j]=block[j][i];
						break;
					default:
						break;
				}
	
			}
		}
		block=tmp_block;
	}
	return block;



void getBlockMap (bool *out_block [4][4], int blockindex, int rot){
	switch(blockindex){
		case 0:
			*out_block=square;
			return;
		case 1:
			*out_block=modBlock(l_shape,false,rot);
			return;
		case 2:
			*out_block=modBlock(l_shape,true,rot);
			return;
		case 3:
			*out_block=modBlock(i_shape,false,rot);
			return;
		case 4:
			*out_block=modBlock(s_shape,false,rot);
			return;
		case 5:
			*out_block=modBlock(s_shape,true,rot);
			return;
		case 6:
			*out_block=modBlock(half_cross,false,rot);
			return;
		default:
			return;
	}
}



